<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Product;
use Illuminate\Support\Str;
use Faker\Generator as Faker;



$factory->define(Product::class, function (Faker $faker) {
    $name=$faker->company;
    return [
        'name' => $name,
        'slug' => $name,
        'price' => random_int(1,10),
    ];
});
