<?php
/**
 * Created by PhpStorm.
 * User: rameezraja
 * Date: 29/11/2018
 * Time: 10:13 PM
 */

namespace App\Repositories;

interface RepositoryInterface
{
    public function all();

    public function create($data);

    public function update($data, $id);

    public function delete($id);

    public function show($id);
}
