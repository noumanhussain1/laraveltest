<?php

namespace Tests\Feature\Http\Controller\Api;

use Faker\Factory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductController extends TestCase
{

//    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
//        $faker=Factory::create();
//        $response = $this->json('POST','/api/products',[
//            'name'=>$name=$faker->company,
//            'slug'=>$name,
//            'price'=>$price=random_int(10,100),
//
//        ]);
//        \Log::info(1,[$response->getContent()]);
//        $response->assertJsonStructure([
//            'id','name','slug','price','created_at'
//        ])
////           ->assertJson([
////               'name'=>$name,
////               'slug'=>$slug,
////               'price'=>$price,
////           ])
//            ->assertStatus(201);
    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function can_return_a_product(){

        $product =$this->create('Product');


        $response =$this->json('GET',"api/products/$product->id");
        $response->assertStatus(200);

    }  /**
     * A basic feature test example.
     *
     * @test
     */
    public function will_fail_with_a_404_if_product_is_not_found(){

        $response =$this->json('GET',"api/products/1");
        $response->assertStatus(200);

    }

}

